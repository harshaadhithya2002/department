import imp
from operator import truediv
from django.db import models
from django.contrib.auth.models import User
import uuid

import datetime

def year_choices():
    return [(r,r) for r in range(1984, datetime.date.today().year+1)]

# Create your models here.

class Profile(models.Model):
    departments=(('ISE','ISE'),('CSE','CSE'),('IT','IT'))
    user=models.OneToOneField(User,on_delete=models.CASCADE,null=True,blank=True)
    roll_no=models.CharField(max_length=30,unique=True,blank=True,null=True)
    dept=models.CharField(max_length=20,choices=departments,blank=True,null=True)
    batch=models.IntegerField(default=datetime.date.today().year, choices=year_choices())
    is_admin=models.BooleanField(default=False)
    created=models.DateTimeField(auto_now_add=True)
    id=models.UUIDField(default=uuid.uuid4,unique=True,primary_key=True,editable=False)

    def __str__(self):
        if self.roll_no:
            return self.roll_no
        else:
            return self.user.username

